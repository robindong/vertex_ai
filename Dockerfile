# Dockerfile
#FROM python:3.9.0
FROM google/cloud-sdk:slim
ENV GOOGLE_APPLICATION_CREDENTIALS="/vertex_ai/mlchapter-dev-api-key.json"

WORKDIR /vertex_ai

COPY requirements.txt mlchapter-dev-api-key.json ./
RUN apt-get update \
    &&  pip3 install --upgrade pip==21.1.3 \
    && pip3 install --no-cache-dir -r requirements.txt

# copy all code
COPY . ./

ENTRYPOINT ["python3"]
CMD ["xgboost/xgboost_trainer/__main__.py"]
