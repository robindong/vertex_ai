PROJECT ?= gcp-wow-rwds-ai-mlchapter-dev
CACHE ?= 1
ZONE ?= us-central1
gcloud_key_file := $(if $(GOOGLE_APPLICATION_CREDENTIALS),$(GOOGLE_APPLICATION_CREDENTIALS),"/tmp/mlchapter-dev-api-key.json")
GCR_URI := $(if $(GCR_URI),$(GCR_URI),gcr.io/$(PROJECT))
tag ?= custom-image-test
toolkit_tag = toolkit-$(tag)
tag_latest := $(tag):latest


gcloud-auth: ## Authenticate with GCS
	@echo "Authenticating with the service account key file"
	gcloud auth activate-service-account --key-file $(gcloud_key_file)
	gcloud config set project $(PROJECT)

build: ## Build docker ci container (GCLOUD_API_KEYFILE and GCR_URI required)
	$(MAKE) gcloud-auth
	docker build -t $(tag) .

publish: ## Publish ci container
	@cat $(gcloud_key_file) | docker login -u _json_key --password-stdin https://$(GCR_URI) && \
	docker tag $(tag) $(GCR_URI)/$(tag) && docker push $(GCR_URI)/$(tag)
	gcloud container images add-tag $(GCR_URI)/$(tag) $(GCR_URI)/$(tag_latest)
	@echo "Docker image: $(GCR_URI)/$(tag)"
