from setuptools import find_packages
from setuptools import setup

setup(
    name='xgboost_trainer',
    version='0.1',
    packages=find_packages(),
    include_package_data=True,
    install_requires=["pyarrow>=4.0.1", "pandas>=1.2.4"],
    description='XGBoost test version 1'
)
