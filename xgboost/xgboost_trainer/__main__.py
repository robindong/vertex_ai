import os
import glob
import subprocess
import pandas as pd
import xgboost as xgb

from pathlib import Path

params3 = {
    'objective': 'reg:squarederror',
    'metric': {'l2', 'l1'},
    'metric_freq': 1,
    'num_leaves': 12,
    'max_depth': 4,
    'min_data_in_leaf': 200,
    'learning_rate': 0.15,
    'feature_fraction': 0.8,
    'bagging_fraction': 0.9,
    'bagging_freq': 1,
    'n_estimators': 2000,
    'verbosity': 1,
    # 'num_threads': 8
    # 'num_threads': 96
    # 'num_threads': 32
    'num_threads': 16 
}

def read_multiple_csvs(path):
    all_files = glob.glob(path + "*.csv")

    lst = []
    index = 0
    for filename in all_files:
        if index == 0:
            df = pd.read_csv(filename)
            index += 1
        else:
            df = pd.read_csv(filename, header=0)
        lst.append(df)

    return pd.concat(lst, axis=0, ignore_index=True)

def load_dataset():
    train_dataset = os.environ.get("AIP_TRAINING_DATA_URI", "")
    eval_dataset = os.environ.get("AIP_VALIDATION_DATA_URI", "")

    Path("./train").mkdir(exist_ok=True, parents=True)
    Path("./eval").mkdir(exist_ok=True, parents=True)

    subprocess.run(f"gsutil cp {train_dataset} ./train/", shell=True)
    subprocess.run(f"gsutil cp {eval_dataset} ./eval/", shell=True)

    df_m = read_multiple_csvs("./train/")
    df_t = read_multiple_csvs("./eval/")

    print(df_m.shape)
    print(df_t.shape)

    train = df_m.drop(['crn', 'ref_dt', 'cut', 'sample_weight'], axis=1)
    test = df_t.drop(['crn', 'ref_dt', 'cut', 'sample_weight'], axis=1)

    tmp = test.values

    # train = train.head(500000)
    # test = train.head(500000)

    train_target = train['target']
    test_target = test['target']
    train = train.drop(['target'], axis=1)
    test = test.drop(['target'], axis=1)
    return train, train_target, test, test_target

def train_model(train, train_target, test, test_target):
    model = xgb.XGBRegressor(**params3)
    model.fit(train, train_target, eval_set=[(train, train_target), (test, test_target)], eval_metric="rmse", verbose=True, early_stopping_rounds=2)
    result = model.evals_result()
    print(result)
    model.save_model("result.model")

if __name__ == "__main__":
    data = load_dataset()
    train_model(*data)
